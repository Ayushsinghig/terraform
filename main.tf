
// tf with custome env var
// export TF_VAR_avail_zone="ap-south-1a"

provider "aws" {
  region     = "ap-south-1"
  access_key = "AKIAUPPBTTPULENHJSII"
  secret_key = "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"
}

variable "cidr_blocks" {
  description = "cidr blocks for VPC and subnets"
  type        = map(string)
}

resource "aws_vpc" "dev-vpc" {
  cidr_block = var.cidr_blocks["vpc"]
  tags = {
    Name = "development-vpc"
    vpc-env : "dev"
  }
}

variable avail_zone {}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id            = aws_vpc.dev-vpc.id
  cidr_block        = var.cidr_blocks["subnet_1"]
  availability_zone = var.avail_zone
  tags = {
    Name = "dev-subnet-1"
  }
}

data "aws_vpc" "existing_vpc" {
  default = true
}


resource "aws_subnet" "dev-subnet-2" {
  vpc_id            = data.aws_vpc.existing_vpc.id
  cidr_block        = var.cidr_blocks["subnet_2"]
  availability_zone = var.avail_zone
  tags = {
    Name = "dev-subnet-2"
  }
}

output "id" {
  value = {
    vpc_id      = aws_vpc.dev-vpc.id
    subnet_1_id = aws_subnet.dev-subnet-1.id
    subnet_2_id = aws_subnet.dev-subnet-2.id
  }
}
